### React Hooks Documentation

#### 1. `useState`
- **Description:** `useState` adalah hook yang digunakan untuk mengelola state dalam komponen fungsional.
- **Penggunaan di Aplikasi:** Digunakan untuk mengelola state `notes`, `selectedNote`, dan `editingNoteId`.

#### 2. `useEffect`
- **Description:** `useEffect` adalah hook yang digunakan untuk mengatur efek samping dalam komponen fungsional.
- **Penggunaan di Aplikasi:** Digunakan untuk memuat catatan dan mengatur referensi input saat `selectedNote` berubah.

#### 3. `useCallback`
- **Description:** `useCallback` adalah hook yang digunakan untuk menghindari pembuatan ulang fungsi dalam komponen fungsional.
- **Penggunaan di Aplikasi:** Digunakan untuk mendefinisikan fungsi `fetchNotes` agar tidak diubah saat `selectedNote` berubah.

#### 4. `useRef`
- **Description:** `useRef` adalah hook yang digunakan untuk mengakses referensi ke elemen DOM atau nilai yang persisten antara rerender komponen.
- **Penggunaan di Aplikasi:** Digunakan untuk mengakses referensi input `titleRef` dan `contentRef`.

#### 5. `useCallback`
- **Description:** `useCallback` adalah hook yang digunakan untuk menghindari pembuatan ulang fungsi dalam komponen fungsional.
- **Penggunaan di Aplikasi:** Digunakan untuk mendefinisikan fungsi-fungsi `handleAdd`, `handleUpdate`, `handleCardEdit`, `handleCardSave`, `handleSubmit`, dan `handleDelete`.

#### 6. `useEffect`
- **Description:** `useEffect` (lagi) digunakan untuk memuat catatan saat komponen dimount dan ketika `selectedNote` berubah.
- **Penggunaan di Aplikasi:** Digunakan untuk memuat catatan saat komponen dimount dan ketika `selectedNote` berubah.

### 7. `useEffect`
- **Description:** `useEffect` (lagi) digunakan untuk mengatur efek samping dalam komponen fungsional.
- **Penggunaan di Aplikasi:** Digunakan untuk mengatur tinggi textarea saat `editingNoteId` berubah.