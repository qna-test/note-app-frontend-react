import React, {useState, useEffect, useCallback, useRef} from 'react';
import { FiEdit2, FiTrash2 } from 'react-icons/fi';
import Masonry from 'react-masonry-component';
import axios from 'axios';
import './App.css';


function App() {
    const [notes, setNotes] = useState([]);
    const [selectedNote, setSelectedNote] = useState(null);
    const titleRef = useRef();
    const contentRef = useRef();
    const [editingNoteId, setEditingNoteId] = useState(null);

    const API_BASE_URL = "http://localhost:8811";

    const fetchNotes = useCallback(() => {
        axios.get(`${API_BASE_URL}/api/notes`)
            .then(response => {
                setNotes(response.data);
            })
            .catch(error => {
                console.error("Failed to fetch notes:", error);
            });
    }, []);

    useEffect(() => {
        if (selectedNote) {
            titleRef.current.value = selectedNote.title;
            contentRef.current.value = selectedNote.content;
        }
        fetchNotes();
    }, [selectedNote]);


    const handleAdd = (note) => {
        axios.post(`${API_BASE_URL}/api/notes`, note)
            .then(() => {
                fetchNotes();
            })
            .catch(error => {
                console.error("Failed to add note:", error);
            });
    };

    const handleUpdate = (id, note) => {
        axios.put(`${API_BASE_URL}/api/notes/${id}`, note)
            .then(() => {
                fetchNotes();
            })
            .catch(error => {
                console.error("Failed to update note:", error);
            });
    };

    const resizeTextarea = (event) => {
        event.target.style.height = 'inherit';
        event.target.style.height = `${event.target.scrollHeight}px`;
    };

    const handleCardEdit = (note) => {
        setSelectedNote(note);
        setEditingNoteId(note.id);

        // A slight delay to ensure the refs are properly updated
        setTimeout(() => {
            titleRef.current.style.height = `${titleRef.current.scrollHeight}px`;
            contentRef.current.style.height = `${contentRef.current.scrollHeight}px`;
        }, 0);
    };

    const handleCardSave = (id) => {
        const updatedNote = {
            title: titleRef.current.value,
            content: contentRef.current.value,
        };

        handleUpdate(id, updatedNote);
        setSelectedNote(null);
        setEditingNoteId(null); // Reset the editing state after saving
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        const newNote = {
            title: titleRef.current.value,
            content: contentRef.current.value,
        };

        if (selectedNote) {
            handleUpdate(selectedNote.id, newNote);
        } else {
            handleAdd(newNote);
        }

        titleRef.current.value = "";
        contentRef.current.value = "";
        setSelectedNote(null);
    };

    const handleDelete = (id) => {
        axios.delete(`${API_BASE_URL}/api/notes/${id}`)
            .then(() => {
                fetchNotes();
            })
            .catch(error => {
                console.error("Failed to delete note:", error);
            });
    };

    return (
        <div className="app">
            <form className="note-form" onSubmit={handleSubmit}>
                <input ref={titleRef} placeholder="Title" />
                <textarea ref={contentRef} placeholder="Content" />
                <button type="submit">Save</button>
            </form>
            <div className="notes-grid">
                {notes.map(note => (
                    <div key={note.id} className="note-card" style={{ backgroundColor: note.color }}>
                        <div className="note-icons">
                            {editingNoteId === note.id ? (
                                <button
                                    type="button"
                                    style={{ alignSelf: 'flex-start', margin: '10px 0' }}
                                    className="card-save-button"
                                    onClick={() => handleCardSave(note.id)}>
                                    Save
                                </button>
                            ): (
                                <>
                                <FiEdit2 onClick={() => handleCardEdit(note)} />
                                <FiTrash2 onClick={() => handleDelete(note.id)} />
                                </>)
                            }
                        </div>
                        {editingNoteId === note.id ? (
                            <>
                                <input defaultValue={note.title} ref={titleRef} />
                                <textarea
                                    defaultValue={note.content}
                                    ref={contentRef}
                                    onChange={resizeTextarea}
                                />
                            </>
                        ) : (
                            <>
                                <h3>{note.title}</h3>
                                <p>{note.content}</p>
                            </>
                        )}
                    </div>
                ))}
            </div>
        </div>
    );
}

export default App;